import bpy
import urllib
import json
import zipfile
import os
import math
import bmesh
from bpy.types import Operator


from . import utils

from bpy.props import (
        StringProperty,
        BoolProperty,
        IntProperty,
        FloatProperty,
        FloatVectorProperty,
        EnumProperty,
        )

tileset = {}

def get_tiles_in_basemap(error_margin):
    
    get_tileset()
    global tileset
    
    bboxes = list()

    basemap = utils.get_basemap_obj()

    shiftLon = basemap.location.x
    shiftLat = basemap.location.y


    lon = bpy.context.scene["longitude"]
    lat = bpy.context.scene["latitude"]

    rd = utils.Rijksdriehoek()
    rd.from_wgs(lat,lon)

    lon = rd.rd_x + shiftLon
    lat = rd.rd_y + shiftLat

    rootmin_x = tileset['root']["transform"][12]
    rootmin_y = tileset['root']["transform"][13]

    lon -= rootmin_x
    lat -= rootmin_y

    sscale = utils.get_scene_scale_m()
    error = error_margin
    worldRect = [lon - sscale[0]/2 - error, lat - sscale[1]/2 - error, lon + sscale[0]/2 + error, lat + sscale[1]/2 + error]

    recursivelyFindBBoxes(tileset['root'], worldRect, bboxes)
    return bboxes
    
def get_tileset():
    global tileset
    if tileset:
        print("tileset already cached")
    else:
        print("downloading and caching tileset")
        url = 'https://3dbag.nl/download/3dtiles/v21031_7425c21b/lod22/tileset.json'

        req = urllib.request.Request(url, None, utils.get_request_header())
        handle = urllib.request.urlopen(req, timeout=10)
        data = handle.read()
        handle.close()

        tileset = json.loads(data)
    

def recursivelyFindBBoxes(item, worldRect, bboxes):
    bbox = item['boundingVolume']['box']
    currentRect = [bbox[0], bbox[1], bbox[0]+bbox[3], bbox[1]+bbox[7]]
    
    
    if True:
        if 'boundingVolume' in item:
            if isRectangleOverlap(currentRect, worldRect):
                if 'content' in item:
                    if 'uri' in item['content']:
                        bboxes.append([currentRect, item['content']['uri']])
        if 'children' in item:
            for child in item['children']:
                recursivelyFindBBoxes(child, worldRect, bboxes)

def isRectangleOverlap(R1, R2):
    if (R1[0]>=R2[2]) or (R1[2]<=R2[0]) or (R1[3]<=R2[1]) or (R1[1]>=R2[3]):
        
        return False
    else:
        return True

    
class op_get_bag(Operator):
    bl_idname = "gis.get_bag"
    bl_label = "Get 3D BAG"
    bl_description = "Download 3D buildings to match your existing basemap"
    bl_options = {'REGISTER', 'UNDO'}
    
    bag_lod: EnumProperty(
        name="Detail",
        description="3D BAG comes in different levels of detail.\nLOD 1.2 is the lowest, with each building being just one shape\nLOD 1.3 can have building split up in different heights\nLOD 2.2 has defined roof shapes",
        items=(
            ('lod12', "LOD 1.2 (low detail)", ""),
            ('lod13', "LOD 1.3 (medium detail)", ""),
            ('lod22', "LOD 2.2 (high detail)", "")
        ),
        default='lod22',
    )
    
    excess_cleanup: EnumProperty(
        name="Excess",
        description="The downloaded 3D BAG tiles are bigger than the basemap. What to do with the excess?",
        items=(
            ('CUT', "Cut away", "This cuts away all the excess geometry outside the basemaps's bounds"),
            ('INCLUDE', "Remove Inclusive", "This keeps all buildings that touch the basemap (some overhang)"),
            ('EXCLUDE', "Remove Exclusive", "This removes all the buildings that are not entirely on the basemap (no overhang)"),
            ('NOTHING', "Do nothing", "Keeps the tiles intact")
        ),
        default='CUT',
    )
    
    error_margin: FloatProperty(
        name="Excess",
        description="(advanced) Sometimes too few tiles are pulled, increase this value until it works.",
        default=500,
    )
    

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=250)

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        if(utils.is_scene_georef()):
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="Get Dutch 3D buildings to", icon='INFO')
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="match your existing basemap")
            layout.separator()
            layout.row()
            layout.row()
            
            row = layout.row()
            row.label(text="Download:", icon='URL')
            row = layout.row()
            row.prop(self, "bag_lod")
            row = layout.row()
            row.prop(self, "error_margin")
            numtiles = len(get_tiles_in_basemap(self.error_margin))
            row = layout.row()             
            row.scale_y = 0.6
            row.label(text="Tiles in basemap: " + str(numtiles))
            
            layout.row()
            layout.row()
                  
            row = layout.row()
            row.label(text="Mesh:", icon='MOD_DISPLACE')
            row = layout.row()
            row.prop(self, "excess_cleanup")
            
        else:
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="Scene has no basemap!", icon='ERROR')
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="use (GIS > Web geodata > Basemap)")

    def execute(self, context):

        print("\n\n########## BLENDER HOOGTEDATA ADDON ##########\n\n")

        utils.ensure_basemap_scale()
        bboxes = get_tiles_in_basemap(self.error_margin)
        
        print("Number of tiles in search: " + str(len(bboxes)))
              

        for b in bboxes:
            
            id = str(b[1])
            id = id[6:-5]
            
            print("\n\nGetting 3D BAG tile: " + str(id))

            url = 'https://data.3dbag.nl/obj/v210908_fd2cee53/3dbag_v210908_fd2cee53_' + id + '.zip'
            print(url)
            
            try:
                req = urllib.request.Request(url, None, utils.get_request_header())
                handle = urllib.request.urlopen(req, timeout=10)
                data = handle.read()
                handle.close()
            
            except Exception as e:
                print(e)
                print("If the exception above is a 404, there's a good chance the tile does not exist / is empty")
            
            tmppath = utils.get_tmp_path()
            
            file_path = tmppath + 'bag3d_' + id +'.zip'
            print("Saving file to: " + file_path)

            zip_file = open(file_path,'wb') 
            zip_file.write(data)
            zip_file.close()
            
            tmpzipfolder = tmppath + 'bag3d_' + id + '/'
            
            print("extract zip file and get the goods (.obj)")

            with zipfile.ZipFile(file_path, 'r') as zip_ref:
                zip_ref.extractall(tmpzipfolder)
                for x in os.listdir(tmpzipfolder):
                    if str(self.bag_lod) in x and x.endswith('obj'):
                        bpy.ops.import_scene.obj(filepath = tmpzipfolder + x, use_split_objects=False, use_split_groups=False, axis_forward = 'Y', axis_up = 'Z')
                    
        
        print("Transforming all OBJ's")
        
        lon = bpy.context.scene["longitude"]
        lat = bpy.context.scene["latitude"]
        
        print("Coordinates: " + str(lon) + ", " + str(lat))

        rd = utils.Rijksdriehoek()
        rd.from_wgs(lat,lon)
        
        bag_objs = [obj for obj in bpy.context.scene.objects if obj.name.startswith("3dbag")]
        
        if(len(bag_objs) == 0):
            return {'CANCELLED'}
        
        bpy.ops.object.select_all(action='DESELECT')        

        for b in bag_objs:
            b.location = (-rd.rd_x ,-rd.rd_y, 0)
            b.select_set(True)
            bpy.context.view_layer.objects.active = b
            
        print("Joining the OBJ's")

        bpy.ops.object.join()
        bpy.context.view_layer.objects.active.name = bpy.context.view_layer.objects.active.name.replace('3dbag', '3d_bag') #to avoid confusion with later added objects.
        
        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True, properties=False)
        
        bpy.ops.object.mode_set(mode = 'EDIT') 
        
        bpy.ops.mesh.select_all(action='SELECT')

        
        basemap = utils.get_basemap_obj()

        cut_x = basemap.location.x
        cut_y = basemap.location.y
        sscale = utils.get_scene_scale_m()
        
        print("Processing joined OBJ according to: " + str(self.excess_cleanup))
        
        if(self.excess_cleanup == 'CUT'):
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.bisect(plane_co=(cut_x - sscale[0]/2, 0, 0), plane_no=(-1, 0, 0), use_fill=False, clear_outer=True)
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.bisect(plane_co=(cut_x + sscale[0]/2, 0, 0), plane_no=(1, 0, 0), use_fill=False, clear_outer=True)
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.bisect(plane_co=(0, cut_y - sscale[1]/2, 0), plane_no=(0, -1, 0), use_fill=False, clear_outer=True)
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.bisect(plane_co=(0, cut_y + sscale[1]/2, 0), plane_no=(0, 1, 0), use_fill=False, clear_outer=True)
        
        elif(self.excess_cleanup == 'NOTHING'):
            print('do nothing')
        
        else:
            ob = bpy.context.object
            me = ob.data
            bm = bmesh.from_edit_mesh(me)
            bm.select_mode = {'VERT'}
            bpy.ops.mesh.select_all(action='DESELECT')
            for i, v in enumerate(bm.verts):
                if v.co.x > cut_x - sscale[0]/2 and v.co.x < cut_x + sscale[0]/2 and v.co.y > cut_y - sscale[1]/2 and v.co.y < cut_y + sscale[1]/2:
                    v.select_set(True)
            if(self.excess_cleanup == 'EXCLUDE'):
                bpy.ops.mesh.select_all(action='INVERT')
            bpy.ops.mesh.select_linked()
            if(self.excess_cleanup == 'INCLUDE'):      
                bpy.ops.mesh.select_all(action='INVERT')
            bpy.ops.mesh.delete(type='VERT')
            bmesh.update_edit_mesh(me)            
        
        bpy.ops.object.mode_set(mode = 'OBJECT') 
  
  
        print("All done!")
        print("\n\n########## # ##########\n\n")
    
        return {'FINISHED'}

    
